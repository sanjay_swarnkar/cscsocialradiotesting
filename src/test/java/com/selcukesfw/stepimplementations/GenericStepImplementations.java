package com.selcukesfw.stepimplementations;

import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.naming.ConfigurationException;

import org.apache.commons.configuration.XMLConfiguration;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.gargoylesoftware.htmlunit.javascript.background.JavaScriptExecutor;
import com.google.common.primitives.Ints;
import com.selcukesfw.alm.utils.ALMUtils;
import com.selcukesfw.db.utilities.DBUtils;
import com.selcukesfw.utilities.CompareImages;
import com.selcukesfw.utilities.DriverFactory;
import com.selcukesfw.utilities.ElementFactory;
import com.selcukesfw.utilities.WebUtils;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class GenericStepImplementations {

XMLConfiguration config;
static String radio1;
static String str1;
static String str2;
static String CompareResult;
static String ParentWindow;
static String TweetText;
	@Before
	public void setUp(Scenario scenario) throws ConfigurationException, org.apache.commons.configuration.ConfigurationException {
		config = new XMLConfiguration("configurations/object-config.xml");
//		if (config.getString("alm_integration") != null && config.getString("alm_integration").equalsIgnoreCase("true")) {
//			Assume.assumeTrue(true);
//		}
	}

	@After
	public void tearDown(Scenario scenario) {
		//below block is to connect to ALM and update the status.
		if (config.getString("alm_integration") != null && config.getString("alm_integration").equalsIgnoreCase("true")) {
			String testCaseNum = ALMUtils.getTestCaseID(scenario.getName());
			String almTestSetId = System.getProperty("almTestSetId");
			System.out.println("almTestSetId:" + almTestSetId);
			System.out.println("Test Status:" + scenario.getStatus());
			System.out.println("ALM Test Case Number:" + testCaseNum);
			if (scenario.getStatus().equalsIgnoreCase("passed")) {
				ALMUtils.setTestCaseStatus(almTestSetId, testCaseNum, "Passed");
			} else {
				ALMUtils.setTestCaseStatus(almTestSetId, testCaseNum, "Failed");
			}
		}
		DriverFactory.quit();
		config.clear();
	}

	
	/**
	 * This method puts the execution on hold for given number of seconds.
	 * @param seconds
	 */
	@Then("I wait for \"([^\"]*)\" seconds.")
	public void i_wait_for_given_seconds(String seconds) {
		try {
			Thread.sleep(new Long(seconds) * 1000);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * This method takes the control to a given URL
	 * @param url
	 * @throws InterruptedException 
	 */
	@Given("I open \"([^\"]*)\" URL in the browser")
	public void i_go_to_URL(String url) throws InterruptedException {
		try {
			String configuredURL = config.getString(url);
			DriverFactory.getDriver().get(configuredURL);
			System.err.println(DriverFactory.getDriver().getTitle());
		} catch (Exception e) {
			DriverFactory.getDriver().get(url);
			Thread.sleep(3000);
		}
	}
	
	
	@Given("I open Application URL in the browser.")
	public void i_open_url() throws InterruptedException {
		try {
			DriverFactory.getDriver().get("http://134.168.24.66:9080/worklightconsole/login.html");
			Thread.sleep(10000);
			
			if(DriverFactory.getDriver().findElement(By.xpath(".//*[@id='j_username']")).isDisplayed())
			{
				System.out.println("Login to Bluemix");
				Thread.sleep(3000);
			DriverFactory.getDriver().findElement(By.xpath(".//*[@id='j_username']")).sendKeys("admin");
			System.out.println("I enter the user name");
			DriverFactory.getDriver().findElement(By.xpath(".//*[@id='j_password']")).sendKeys("admin");
			System.out.println("I enter the pwd");
			DriverFactory.getDriver().findElement(By.xpath(".//*[@id='loginButton']")).click();
			}
			Thread.sleep(3000);
			DriverFactory.getDriver().manage().deleteAllCookies();
			DriverFactory.getDriver().get("http://134.168.24.66:9080/_MobileBrowserSimulator/index.html?webpage=/MobileFirstStarter/apps/services/preview/CSCSocialRadio/android/1.0/&platform;android");
			System.err.println(DriverFactory.getDriver().getTitle());
		} catch (Exception e) {
			DriverFactory.getDriver().get("http://134.168.24.60:9080/worklightconsole/login.html");
			Thread.sleep(10000);
			
			if(DriverFactory.getDriver().findElement(By.xpath(".//*[@id='j_username']")).isDisplayed())
			{
				System.out.println("Login to Bluemix");
			DriverFactory.getDriver().findElement(By.xpath(".//*[@id='j_username']")).sendKeys("admin");
			DriverFactory.getDriver().findElement(By.xpath(".//*[@id='j_password']")).sendKeys("admin");
			DriverFactory.getDriver().findElement(By.xpath(".//*[@id='loginButton']")).click();
			}
			Thread.sleep(3000);
			DriverFactory.getDriver().manage().deleteAllCookies();
			DriverFactory.getDriver().get("http://134.168.24.60:9080/_MobileBrowserSimulator/index.html?webpage=/MobileFirstStarter/apps/services/preview/CloudantSampleApp/android/1.0/&platform;android");
			System.err.println(DriverFactory.getDriver().getTitle());	}
	}
	
	
	@Given("I go to \"([^\"]*)\" URL later")
	public void i_go_to_URL_later(String url) {
		try {
			String configuredURL = config.getString(url);
			DriverFactory.getDriver().get(configuredURL);
		} catch (Exception e) {
			DriverFactory.getDriver().get(url);
		}
	}

	/* START: Handle Check boxes */

	/**
	 * This method selects(checks) an unselected (unchecked) checkbox
	 * 
	 * @param identifier (identifier is calculated based on ObjectIdentityConfig.properties)
	 */
	@Then("I select \"([^\"]*)\" checkbox.")
	public void i_select_checkbox(String identifier) {
		if (!ElementFactory.getElement(identifier).isSelected()) {
			ElementFactory.getElement(identifier).click();
		}
	}

	/**
	 * This method unselects (unchecks) a selected (checked) checkbox
	 * 
	 * @param identifier (identifier is calculated based on ObjectIdentityConfig.properties)
	 */
	@Then("I unselect \"([^\"]*)\" checkbox.")
	public void i_unselect_checkbox(String identifier) {
		if (ElementFactory.getElement(identifier).isSelected()) {
			ElementFactory.getElement(identifier).click();
		}
	}

	/* END: Handle Check boxes */
	
	/* START: Handle Buttons */
	/**
	 * This method clicks on a button based on the HTML identifier provided.
	 * @param buttonIdentifier (identifier is calculated based on ObjectIdentityConfig.properties)
	 */
	@And("I click on \"([^\"]*)\" button.")
	public void i_click_on_button(String buttonIdentifier) {
		ElementFactory.getElement(buttonIdentifier).click();
	}
	
	
/*	@And("I scroll down the Page")
	public void i_scroll_down_the_page() {
		JavaScriptExecutor jsx=(JavaScriptExecutor).DriverFactory.getDriver();
	}*/
	
	@And("I select  \"([^\"]*)\" radio button for \"([^\"]*)\" field.")
	public void i_select_radio1_button(String buttonIdentifier, String value) {
		System.out.println(buttonIdentifier);
		DriverFactory.getDriver().findElement(By.id(buttonIdentifier)).click();
		
		
	}
	
	@And("I click on \"([^\"]*)\" Field.")
	public void i_click_on_Field(String buttonIdentifier) {
		ElementFactory.getElement(buttonIdentifier).click();
	}

	/* End: Handle Buttons */
	
	/* Start: Handle links */
	/**
	 * This method clicks on a hyper link 
	 * @param linkIdentifier (identifier is calculated based on ObjectIdentityConfig.properties)
	 */
	@Then("I click on \"([^\"]*)\" link")
	public void i_click_on_link(String linkIdentifier) {
		ElementFactory.getElement(linkIdentifier).click();
	}
	
	/**
	 * This method clicks on a hyper link and focuses on popup window
	 * @param linkIdentifier (identifier is calculated based on ObjectIdentityConfig.properties)
	 * @throws InterruptedException 
	 */
	
	
	@Then("I check if the no. of tweets is less than or equal to 10")
	public void i_check_no_of_tweets() throws InterruptedException {
		
	int NoofTweets=DriverFactory.getDriver().findElements(By.xpath(".//*[@id='list']/tr[.]/td")).size();
	Thread.sleep(3000);
	System.out.println(NoofTweets);
try{
	if(NoofTweets==0)
	{
		Assert.assertTrue("No. of Tweets is 0",NoofTweets==0);
	}
	if(NoofTweets<=10)
	{
		Assert.assertTrue("No. of Tweets is :"+NoofTweets,NoofTweets<=10);
	}
	
	if(NoofTweets>10)
	{
		Assert.assertTrue("No. of Tweets is more than 10",(NoofTweets>10));
	}
	
}
catch(Exception e){
	Assert.assertTrue("No. of Tweets is 0",(NoofTweets==10));
}
	}
	
	@Then("I check if the tweet contains \"([^\"]*)\" and \"([^\"]*)\" keywords for CSC Trends tweets")
	public void i_check_if_tweet_contains_words_csc(String csc1,String SearchWord) {
		csc1=csc1.toLowerCase();
		SearchWord=SearchWord.toLowerCase();	
	int NoofTweets=DriverFactory.getDriver().findElements(By.xpath(".//*[@id='list']/tr[.]/td")).size();
	try{
for(int i=1;i<=NoofTweets;i++)	
{
	TweetText=DriverFactory.getDriver().findElement(By.xpath(".//*[@id='list']/tr["+i+"]/td")).getText();
	TweetText=TweetText.toLowerCase();
	if(TweetText.contains(csc1))
	{
		if(TweetText.contains(SearchWord))
		{
			Assert.assertTrue("Tweet contains "+SearchWord,TweetText.contains(SearchWord));	
		}
		else
		{
			Assert.assertTrue("Tweet doesn't contain "+SearchWord+" at position"+i,!(TweetText.contains(SearchWord)));	
		}
	}
	else
	{
		Assert.assertTrue("Tweet doesn't contain CSC",!(TweetText.contains(csc1)));
	}
}
}
catch(Exception e){
	Assert.assertTrue("No. of Tweets is 0",(NoofTweets==10));
}
	}
	
	@Then("I check if the tweet does not contain \"([^\"]*)\" but contains \"([^\"]*)\" keywords for Global Tech Trends tweets")
	public void i_check_if_tweet_not_contain_words_csc(String csc1,String SearchWord) {
		csc1=csc1.toLowerCase();
		SearchWord=SearchWord.toLowerCase();	
	int NoofTweets=DriverFactory.getDriver().findElements(By.xpath(".//*[@id='list']/tr[.]/td")).size();
	try{
for(int i=1;i<=NoofTweets;i++)	
{
	TweetText=DriverFactory.getDriver().findElement(By.xpath(".//*[@id='list']/tr["+i+"]/td")).getText();
	TweetText=TweetText.toLowerCase();
	if(!(TweetText.contains(csc1)))
	{
		if(TweetText.contains(SearchWord))
		{
			Assert.assertTrue("Tweet contains "+SearchWord,TweetText.contains(SearchWord));	
		}
		else
		{
			Assert.assertTrue("Tweet doesn't contain "+SearchWord+" at position"+i,!(TweetText.contains(SearchWord)));	
		}
	}
	else
	{
		Assert.assertTrue("Tweet contains CSC",TweetText.contains(csc1));
	}
}
}
catch(Exception e){
	Assert.assertTrue("No. of Tweets is 0",(NoofTweets==10));
}
	}
	@Then("I Select \"([^\"]*)\"")
	public void i_select_Searchwords(String SearchWord) {
	DriverFactory.getDriver().findElement(By.xpath("//*[text()='"+SearchWord+"']")).click();
	}	
	
	@Then("I click on \"([^\"]*)\" and switch to popup")
	public void i_click_on_link_and_focus_popup(String linkIdentifier) {
		String parentwindowhandle = DriverFactory.getDriver().getWindowHandle();
		ElementFactory.getElement(linkIdentifier).click();
		WebUtils.switchpopups(parentwindowhandle,DriverFactory.getDriver());
	}	
	@Then("I switch to popup")
	public void switch_to_popup() {
		String parentwindowhandle = DriverFactory.getDriver().getWindowHandle();
		WebUtils.switchpopups(parentwindowhandle,DriverFactory.getDriver());
	}
	
	/* End: Handle links */
	/**
	 * This method clicks on a hyper link 
	 * @param linkIdentifier (identifier is calculated based on ObjectIdentityConfig.properties)
	 */
	@Then("I click on \"([^\"]*)\" hyperlink")
	public void i_click_onLink(String linkIdentifier) {
		DriverFactory.getDriver().findElement(By.linkText(linkIdentifier)).click();
		
	}
	
	/* End: Handle links */
	
	/* Start: Handle text boxes */
	/**
	 * This method enters given 'value' into the text box
	 * @param value
	 * @param identifier (identifier is calculated based on ObjectIdentityConfig.properties)
	 */
	@And("I enter \"([^\"]*)\" into \"([^\"]*)\" text field.")
	public void i_enter_value_into_textfield(String value, String identifier) {
		ElementFactory.getElement(identifier).clear();
		ElementFactory.getElement(identifier).sendKeys(value);
	}
	
	@And("I enter password into \"([^\"]*)\" text field.")
	public void i_enter_password_into_textfield(String identifier) {
		ElementFactory.getElement(identifier).clear();
		ElementFactory.getElement(identifier).sendKeys("rajkumariasd123");
	}
	@And("I enter \"([^\"]*)\" into \"([^\"]*)\" number field.")
	public void i_enter_value_into_numberfield(String value, String identifier) 
	{
		ElementFactory.getElement(identifier).sendKeys(Keys.UP);
		ElementFactory.getElement(identifier).sendKeys(Keys.UP);
		ElementFactory.getElement(identifier).sendKeys(Keys.UP);
	}
	
	@Then("I enter current date in \"([^\"]*)\" into \"([^\"]*)\" text field")
	public void enterCurrentDateIntoTextField(String dateFormat, String identifier)
	{
		if(dateFormat.length() > 10)
		{
			Assert.fail("Please enter valid date format. Eg: mm/dd/yyyy");
		}
		//Date format is expected in dd/mm/yyyy or mm/dd/yyyy. dd and yyyy should be lower case. mm could be in upper or lower case.
		dateFormat = dateFormat.replaceAll("mm", "MM");
		String currentDate = new SimpleDateFormat(dateFormat).format(new Date());
		ElementFactory.getElement(identifier).clear();
		ElementFactory.getElement(identifier).sendKeys(currentDate);
	}
	
	@And("I enter \"([^\"]*)\" appended with timestamp into \"([^\"]*)\" text field")
	public void i_enter_value_with_timestamp_into_textfield(String value, String identifier) {
		java.util.Date date= new java.util.Date();
		ElementFactory.getElement(identifier).clear();
		ElementFactory.getElement(identifier).sendKeys(value+date.getTime());
	}
	
	@And("I enter \"([^\"]*)\" into \"([^\"]*)\" field and click tab")
	public void enter_text_field_value_press_tab(String value, String identifier) {
		ElementFactory.getElement(identifier).clear();
		ElementFactory.getElement(identifier).sendKeys(value);
		ElementFactory.getElement(identifier).sendKeys(Keys.TAB);
		
	}
	
	
	/* End: Handle text boxes */
	
	@Then("I select \"([^\"]*)\" from \"([^\"]*)\" select box")
	public void select_value_from_selectbox(String value, String selectboxIdentifier) throws InterruptedException {
		new Select(ElementFactory.getElement(selectboxIdentifier)).selectByVisibleText(value);
	}
	
	/* Start: Handle Radio Buttons */
	/**
	 * This method takes name and value attributes to select a radio button.
	 * @param value
	 * @param name
	 */
	@And("I select \"([^\"]*)\" radio button for \"([^\"]*)\" field.")
	public void i_select_radio_button(String value, String name) {
		final List<WebElement> radios = DriverFactory.getDriver().findElements(By.name(name));
	    for (WebElement radio : radios) {
	        if (radio.getAttribute("id").equals("rFemale")) {
	            radio.click();
	        }
	    }
	}
	@And("I select \"([^\"]*)\" card radio button for \"([^\"]*)\" field.")
	public void i_select_cardradio_button(String value2, String name) {
		final List<WebElement> radios = DriverFactory.getDriver().findElements(By.name(name));
	    for (WebElement radio : radios) {
	        if (radio.getAttribute("id").equals(value2)) {
	            radio.click();
	        }
	    }
	} 
	@And("I verify if my FB account is already logged in or not")
	public void i_FB_Verify() {
		try {
			DriverFactory.getDriver().get("https://www.facebook.com/");
			Thread.sleep(10000);
			
			if(DriverFactory.getDriver().findElement(By.id("userNavigationLabel")).isDisplayed())
			{
				System.out.println("Login to Bluemix");
				Thread.sleep(3000);
			DriverFactory.getDriver().findElement(By.xpath(".//*[@id='j_username']")).sendKeys("admin");
			System.out.println("I enter the user name");
			DriverFactory.getDriver().findElement(By.xpath(".//*[@id='j_password']")).sendKeys("admin");
			System.out.println("I enter the pwd");
			DriverFactory.getDriver().findElement(By.xpath(".//*[@id='loginButton']")).click();
			}
			Thread.sleep(3000);
			DriverFactory.getDriver().manage().deleteAllCookies();
			DriverFactory.getDriver().get("http://20.201.142.74:10080/_MobileBrowserSimulator/index.html?webpage=/CSCsocialradio/apps/services/preview/CloudantSampleApp/android/1.0/&platform;android");
			System.err.println(DriverFactory.getDriver().getTitle());
		} catch (Exception e) {
	    }
	} 
	/* End: Handle Radio Buttons */
	
	/**
	 * This method creates and returns a Selenium WebElement based on the identifier provided.
	 * @param identifier (identifier is calculated based on ObjectIdentityConfig.properties)
	 * @return WebElement
	 */
	public static By getBy(ArrayList<String> objKeySet) {

		By by;
		try {
			Method method = By.class.getMethod(objKeySet.get(0), String.class);
			by = (By) method.invoke(By.class, objKeySet.get(1));
		} catch (Exception NoSuchElementException) {
			return null;
		}
		return by;
	}

	
	@Then("^close browser window$")
	public void close_browser_window() {
		DriverFactory.getDriver().close();
	}
	
	@And("I compare \"([^\"]*)\" image displayed on the web page with \"([^\"]*)\" image")
	public void compare_images(String webUrl, String imageLocalNameExtn) {
		boolean imageMatched = new CompareImages().compareImages(webUrl, imageLocalNameExtn);
		Assert.assertTrue("Images did not match",imageMatched);
	}
	@And("I verify if Tweets are updated after 1 minute")
	public void compare_tweets() throws InterruptedException {
	String str1=DriverFactory.getDriver().findElement(By.xpath(".//*[@id='list']/tr[1]/td")).getText();
	
	Thread.sleep(60000);
	String str2=DriverFactory.getDriver().findElement(By.xpath(".//*[@id='list']/tr[1]/td")).getText();
	
	if(str1.contentEquals(str2))
	{
		System.out.println("Tweets not Updated");
	}
	
	else
	{
		System.out.println("Tweets  Updated");
	}

	}
	
	
	@And("I Fetch the Latest tweets")
	public String GetText() throws InterruptedException {
	 str1=DriverFactory.getDriver().findElement(By.xpath(".//*[@id='list']/tr[1]/td")).getText();
	return str1;
	}
	
	@And("I check the Latest tweets after waiting for 1 minute")
	public String GetText1() throws InterruptedException {
	 str2=DriverFactory.getDriver().findElement(By.xpath(".//*[@id='list']/tr[1]/td")).getText();
	return str2;
	}
	
	@And("I verify if tweets are updated")
	public String Compare2() throws InterruptedException {
	 if(str1.contentEquals(str2))
	 {
		  CompareResult="Tweets not updated";
	 }
	 else
	 {
		 CompareResult="Tweets updated";
	 }
	return CompareResult;
	}
	@And("I upload \"([^\"]*)\" file into \"([^\"]*)\" field")
	public void upload_file_into_field(String fileCompletePath, String inputFieldIdentifier) {
		ElementFactory.getElement(inputFieldIdentifier).sendKeys(fileCompletePath);
	}

	@And("I should see \"([^\"]*)\" on the page$")
	public void verify_text_matching(String text) {
		
		//DriverFactory.getDriver().switchTo().frame(DriverFactory.getDriver().findElement(By.xpath("(//iframe)[1]")));
		String bodyText = DriverFactory.getDriver().findElement(By.tagName("body")).getText();
		Assert.assertTrue("'"+text+"' not found on the page", bodyText.contains(text));
	}
	
	@And("\"([^\"]*)\" should appear on the page$")
	public void verify_text1_matching(String text) {
		String bodyText = DriverFactory.getDriver().findElement(By.tagName("body")).getText();
		Assert.assertTrue("'"+text+"' not found on the page", bodyText.contains(text));
	}
	
	@And("I should not see \"([^\"]*)\" on the page")
	public void verify_text_not_matching(String text) {
		
		DriverFactory.getDriver().switchTo().frame(DriverFactory.getDriver().findElement(By.xpath("(//iframe)[1]")));
		String bodyText = DriverFactory.getDriver().findElement(By.tagName("body")).getText();
		Assert.assertTrue("'"+text+"' not found on the page", (!(bodyText.contains(text))));
	}
	
	@Then("I should see \"([^\"]*)\" displayed on the page")
	public void fieldShouldBedisplayed1(String fieldIdentifier1){
		Assert.assertTrue(fieldIdentifier1+" is expected to be displayed, but it is not displayed", ElementFactory.getElement(fieldIdentifier1).isDisplayed());
	}
	
	/**
	 * This method enters the values into multiple text fields
	 * @param seconds
	 */
	@Then("I fill multiple text fields as per below table:$")
	public void fill_multiple_text_feidls(List<Map<String, String>> values) {
		for(Map<String, String> map : values)
		{
			String fieldName = map.get("fieldName");
			String value = map.get("value");
			i_enter_value_into_textfield(value,fieldName);
		}
	}
	
	@Then("I Should see \"([^\"]*)\" is disabled")
	public void fieldShouldBeDisabled(String fieldIdentifier){
		Assert.assertTrue(fieldIdentifier+" is expected to be disabled, but it is found enabled", !(ElementFactory.getElement(fieldIdentifier).isEnabled()));
	}
	@Then("I Switch to Mobile View")
	public void SwitchToMobileView(){
		DriverFactory.getDriver().switchTo().frame(DriverFactory.getDriver().findElement(By.xpath("(//iframe)[1]")));
	}
	
	@Then("I delete the cookies")
	public void IDeleteCookies(){
		DriverFactory.getDriver().manage().deleteAllCookies();
	}
	@Then("I Switch to FaceBook Login Window and Login Facebook by providing Credentials")
	public void SwitchToFBLogin() throws InterruptedException{
		try{
		Set<String> WindowId=DriverFactory.getDriver().getWindowHandles();
		java.util.Iterator<String> IT=WindowId.iterator();
		ParentWindow=IT.next();
		String ChildWindow=IT.next();
		DriverFactory.getDriver().switchTo().window(ChildWindow);
		//driver.switchTo().alert();
		System.out.println("Hi");
		Thread.sleep(5000);
		Thread.sleep(5000);
		//	driver.findElement(By.xpath(".//*[@id='email']")).sendKeys("romita@gmail.com");
		if(DriverFactory.getDriver().findElement(By.xpath(".//*[@id='email']")).isDisplayed())
		{
		DriverFactory.getDriver().findElement(By.xpath(".//*[@id='email']")).sendKeys("Socialradio123@gmail.com");
		}
		DriverFactory.getDriver().findElement(By.xpath(".//*[@id='pass']")).sendKeys("Password123!@#");
		DriverFactory.getDriver().findElement(By.xpath(".//*[@id='u_0_2']")).click();
			Thread.sleep(5000);
			try{
				DriverFactory.getDriver().findElement(By.name("__CONFIRM__")).click();
			}
			catch(Exception e)
			{
				System.out.println("User will be continued to Login as FB username");
			}
			DriverFactory.getDriver().switchTo().window(ParentWindow);
	}
	catch(Exception e)
	{
		
	}
	}
	
	@Then("I Switch to Parent Window Again")
	public void SwitchToParentWindow() throws InterruptedException{
	String WindowId=DriverFactory.getDriver().getWindowHandle();
	DriverFactory.getDriver().switchTo().window(WindowId);
		Thread.sleep(5000);
	
		//DriverFactory.getDriver().switchTo().frame(DriverFactory.getDriver().findElement(By.xpath("(//iframe)[1]")));
	}
	@Then("I Should see CSCLogo displayed on the Page")
	public void CScLogoShouldBeDisplayed(){
	//	DriverFactory.getDriver().switchTo().frame(DriverFactory.getDriver().findElement(By.xpath("//iframe")));
		Boolean str=DriverFactory.getDriver().findElement(By.xpath(".//*[@src='img/csc_rgb_pos.jpg']")).isDisplayed();
		Assert.assertTrue(" is expected to be displayed, but it is not found ", DriverFactory.getDriver().findElement(By.xpath(".//*[@src='img/csc_rgb_pos.jpg']")).isDisplayed());
	}
	
	@Then("I Should see Tap To CSC Tweets displayed on the Page")
	public void CScTweets(){
	//	DriverFactory.getDriver().switchTo().frame(DriverFactory.getDriver().findElement(By.xpath("//iframe")));
		Boolean str=DriverFactory.getDriver().findElement(By.xpath("//*[contains(text(),'Tap for CSC Tech Tweets')]")).isDisplayed();
		Assert.assertTrue(" is expected to be displayed, but it is not found ", DriverFactory.getDriver().findElement(By.xpath("//*[contains(text(),'Tap for CSC Tech Tweets')]")).isDisplayed());
	}
	
	@Then("I Should see NavButton displayed on the Page")
	public void NavButton(){
		//DriverFactory.getDriver().switchTo().frame(DriverFactory.getDriver().findElement(By.xpath("//iframe")));
		Boolean str=DriverFactory.getDriver().findElement(By.xpath("(//*[@class='button button-icon button-clear ion-navicon'])[2]")).isDisplayed();
		Assert.assertTrue(" is expected to be displayed, but it is not found ", DriverFactory.getDriver().findElement(By.xpath("(//*[@class='button button-icon button-clear ion-navicon'])[2]")).isDisplayed());
	}
	@Then("I Should see radioIcon displayed on the Page")
	public void RadioIcon(){
		//DriverFactory.getDriver().switchTo().frame(DriverFactory.getDriver().findElement(By.xpath("//iframe")));
		Boolean str=DriverFactory.getDriver().findElement(By.xpath(".//*[@src='img/Inkscape-CreateIcon-radio-en.png']")).isDisplayed();
		Assert.assertTrue(" is expected to be displayed, but it is not found ", DriverFactory.getDriver().findElement(By.xpath(".//*[@src='img/Inkscape-CreateIcon-radio-en.png']")).isDisplayed());
	}
	
	@Then("I Should see \"([^\"]*)\" is enabled")
	public void fieldShouldBeEnabled(String fieldIdentifier){
		Assert.assertTrue(fieldIdentifier+" is expected to be enabled, but it is found disabled", ElementFactory.getElement(fieldIdentifier).isEnabled());
	}
	
	@Then("I select below options in \"([^\"]*)\" multi select box")
	public void selectMultipleOptionsFromMultiSelectBox(String multiSelectIdentifier, List<String> options){
		Select selections = new Select(ElementFactory.getElement(multiSelectIdentifier));
		for(String optionValue : options)
		{
			selections.selectByVisibleText(optionValue);
		}
		
	}
	
	@Then("I should see \"([^\"]*)\" table is sorted based on column number \"([^\"]*)\" in \"([^\"]*)\" order$")
	public void checkTableValuesSortedAscending(String tableIdentifier, String columnNumber, String order)
	{
		WebElement table = ElementFactory.getElement(tableIdentifier);
        List<WebElement> rows = table.findElements(By.tagName("tr"));
        List<String> ValuesOfReqdColumn = new ArrayList<String>();
        int columnNum = new Integer(columnNumber) - 1;

        for(int i=1;i<rows.size()-1;i++)
        {
        	WebElement row = rows.get(i);
        	List<WebElement> TDs = row.findElements(By.tagName("td"));
        	if(TDs.size() == 0)
        		continue;
        	WebElement reqColumnElement = TDs.get(columnNum);
        	if(reqColumnElement != null)
        	{
        		ValuesOfReqdColumn.add(reqColumnElement.getText());
        	}
        }
        if(order != null && order.equalsIgnoreCase("descending")){
        	Assert.assertTrue("'"+tableIdentifier+"' table is not sorted in descending order by Column Number:"+columnNumber,!isSorted(ValuesOfReqdColumn));
        }else{
        	Assert.assertTrue("'"+tableIdentifier+"' table is not sorted in ascending order by Column Number:"+columnNumber,isSorted(ValuesOfReqdColumn));
        }
        	
        
	}
	
	public boolean isSorted(List<String> list)
	{
	    boolean sorted = true;        
	    for (int i = 1; i < list.size(); i++) {
	        if (list.get(i-1).compareTo(list.get(i)) > 0) sorted = false;
	    }
	    return sorted;
	}
	
	@Then("I compare \"([^\"]*)\" table contents with \"([^\"]*)\" query with below options:")
	public void compareTableDataWithQueryResults(String tableIdentifier, String queryIdentifier, Map<String,String> options)
	{
		
		List<String> stringDBResultRows = DBUtils.getStringResultRows(queryIdentifier);
		
		String ignoreRows = "";
		String ignoreColumns = "";
		if(options != null)
		{
			ignoreRows = options.get("ignore rows");
			ignoreColumns = options.get("ignore columns");
		}
		int[] rowsToIgnore = stringToIntArray(ignoreRows);
		int[] columnsToIgnore = stringToIntArray(ignoreColumns);
		
		WebElement table = ElementFactory.getElement(tableIdentifier);
		List<WebElement> rows = table.findElements(By.tagName("tr"));
		int j=-1;
		for(int i =0; i<rows.size(); i++)
		{
			j++;
			if(Ints.contains(rowsToIgnore, i+1))
			{
				j--;
				continue;
			}
			String rowUIText = WebUtils.getTableRowAsCommaSeperatedString(rows.get(i), columnsToIgnore);
			String rowDBText = stringDBResultRows.get(j);
			if(!(rowUIText.equalsIgnoreCase(rowDBText)))
			{
				Assert.fail();
			}
		}
	}
	
	private int[] stringToIntArray(String commaSeparatedStringValue)
	{
		String[] items = commaSeparatedStringValue.replaceAll("\\[", "").replaceAll("\\]", "").split(",");
		int[] intArray = new int[items.length];

		for (int i = 0; i < items.length; i++) {
		    try {
		    	intArray[i] = Integer.parseInt(items[i]);
		    } catch (NumberFormatException nfe) {};
		}
		return intArray;
	}
	
	@Then("I click OK button in popup window")
	public void clickOkOnPopUp()
	{
		DriverFactory.getDriver().switchTo().alert().accept();
	}
	
/*	@Then("I test htmlunit step")
	public void getElements() throws Exception {
		
		final WebClient webClient = new WebClient(BrowserVersion.FIREFOX_24, "20.201.204.111", 80);
		System.out.println("coming here 1");
	    //set proxy username and password 
	    final DefaultCredentialsProvider credentialsProvider = (DefaultCredentialsProvider) webClient.getCredentialsProvider();
	    credentialsProvider.addCredentials("vbeerakam", "Indian~123");
	    System.out.println("coming here 2");
	    final HtmlPage page = webClient.getPage("http://htmlunit.sourceforge.net");
	    Assert.assertEquals("HtmlUnit - Welcome to HtmlUnit", page.getTitleText());
	    System.out.println("coming here 3");
//	    webClient.closeAllWindows();
		
		
		System.out.println("coming here 1");
//	    final WebClient webClient = new WebClient();
	    System.out.println("coming here 2");
	    final HtmlPage page1 = webClient.getPage("https://accounts.coursera.org/signup");
	    System.out.println("coming here 3");
	    Assert.assertEquals("Coursera.org", page1.getTitleText());
	    System.out.println("coming here 4");
//	    page.getHtmlElementById("coursera-signup-fullname").setNodeValue("Hello world");
	    Thread.sleep(10000);
//	    final HtmlAnchor anchor = page.getAnchorByName("anchor_name");
	    System.out.println("coming here 5");
	    webClient.closeAllWindows();
	    System.out.println("coming here 6");
	}*/
	
	
}
