Feature: SocialRadio App Launch and UI validation
@SocialRadio
  Scenario: Social radio App Launch and UI validation
  
  Given I open Application URL in the browser.
  Then I wait for "5" seconds.
  Then I Switch to Mobile View
  Then I should see "CSC Social Radio" on the page
  Then I should see "Socialradio.FBButton" displayed on the page
  Then I should see "Socialradio.GoogleButton" displayed on the page
  Then I should see "Socialradio.TwitterButton" displayed on the page
  Then I wait for "5" seconds.