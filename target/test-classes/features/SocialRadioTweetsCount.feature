Feature:  Social radio CSC Trends Tweets Count
@SocialRadio
  Scenario Outline:  Social radio CSC Trends tweets Count
  
  Given I open Application URL in the browser.
  Then I wait for "5" seconds.
  Then I Switch to Mobile View
  Then I click on "Socialradio.FBButton" button.
  Then I wait for "5" seconds.
  Then I Switch to FaceBook Login Window and Login Facebook by providing Credentials
  Then I Switch to Mobile View
  Then I click on "Socialradio.NavButton" button.
  Then I wait for "2" seconds.
  Then I should see "Home" on the page
  Then I should see "CSC Tech Trends" on the page
  Then I should see "Tech Trends" on the page
  Then I click on "CSC Tech Trends" hyperlink
  Then I wait for "2" seconds.
  Then I should see "IOT" on the page
  Then I should see "Big Data" on the page
  Then I should see "DevOps" on the page
  Then I should see "Cloud" on the page
  Then I should see "Agile" on the page  
  Then I wait for "2" seconds.
  Then I Select "<searchWord>"
  Then I wait for "20" seconds.
  Then I check if the no. of tweets is less than or equal to 10
  Then I wait for "5" seconds. 
  
    Examples:
  |searchWord|
  |IOT| 

