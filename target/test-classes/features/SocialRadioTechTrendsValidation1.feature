Feature:  Social radio Tech Trends validation
@SocialRadio
  Scenario:  Social radio Tech Trends validation
  
  Given I open Application URL in the browser.
  Then I wait for "5" seconds.
  Then I Switch to Mobile View
  Then I wait for "2" seconds.
  Then I click on "Socialradio.FBButton" button.
  Then I wait for "5" seconds.
  Then I Switch to FaceBook Login Window and Login Facebook by providing Credentials
  Then I Switch to Mobile View
  Then I click on "Socialradio.NavButton" button.
  Then I wait for "2" seconds.
  Then I should see "Home" on the page
  Then I should see "CSC Tech Trends" on the page
  Then I should see "Global Tech Trends" on the page
  Then I click on "Global Tech Trends" hyperlink
  Then I wait for "2" seconds.
  Then I should see "IOT" on the page
  Then I should see "Big Data" on the page
  Then I should see "DevOps" on the page
  Then I should see "Cloud" on the page
  Then I should see "Agile" on the page 
  Then I wait for "5" seconds.